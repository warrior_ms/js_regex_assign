// Regular expression for valid email
var reg=/^(([^+-<>()\[\]\\.,;:\s@"]+([0-9]*)(\.[^+-<>()\[\]\\.,;:\s@"]+)*))([a-zA-Z]+)@consultadd.com$/;

// /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/

function validate()
{
    var email = document.getElementById("email");
    if(reg.test(email))
    {

        sub.style.visibility = "visible";
    }
    else
    {
        err.style.visibility = "visible";
    }
}

// Getting access to "div" attribute in document
var doc = document.getElementById("js_form");

// Adding form to the divison in document
var form = document.createElement('form');
form.setAttribute("method", "post");
doc.appendChild(form);

// Adding an input field in the form
var inp_field = document.createElement('input');
inp_field.setAttribute("type", "text");
inp_field.setAttribute("id", "email");
inp_field.setAttribute("onblur", "validate()");
form.appendChild(inp_field);

linebreak = document.createElement('br');
document.body.appendChild(linebreak);

// Adding a para for displaying error
var err = document.createElement('input');
err.setAttribute("type", "text");
err.setAttribute("value", "Incorrect email");
err.setAttribute("id", "err");
err.style.visibility="hidden";
err.style.color="red";
err.style.border="white";
form.appendChild(err);

// Adding the submit button
var sub_field = document.createElement('input');
sub_field.setAttribute("type", "submit");
sub_field.setAttribute("id", "sub");
sub_field.style.visibility="hidden";
form.appendChild(sub_field);